# JsCrashCourse

## This is a course intended for experienced programmers in other languages (other than JS) that want to take a deeper dive in the JavaScript world.

### Most of the examples and descriptions are just copied so this is not an original work, just arranged in a way for an experienced developer to go through faster and get the most important concepts.
