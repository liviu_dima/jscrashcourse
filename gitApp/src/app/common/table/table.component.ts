import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IColumn } from 'src/app/types/table/IColumn';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.pug',
  styleUrls: ['./table.component.styl']
})
export class TableComponent implements OnInit {

  @Input() tableHead: string[];
  @Input() tableBody: IColumn[][];

  @Output() prevClicked = new EventEmitter<boolean>();
  @Output() nextClicked = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
    console.log(this.tableBody);
  }

  getPreviousPage() {
    this.prevClicked.emit(true);
  }

  getNextPage() {
    this.nextClicked.emit(true);
  }
}
