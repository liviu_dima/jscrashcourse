import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';

// Modules
import { SharedModule } from './shared.module';
import { AppRoutingModule } from './app.router.module';
import { CoreModule } from './core/core.module';

// Pages
import { UsersComponent } from './pages/users/users.component';
import { HomeComponent } from './pages/home/home.component';
import { UserComponent } from './pages/user/user.component';
import { ReposComponent } from './pages/user/repos/repos.component';
import { CommonComponentsModule } from './common/common.module';

@NgModule({
  declarations: [
    AppComponent,
    // Pages
    UsersComponent,
    HomeComponent,
    UserComponent,
    ReposComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    AppRoutingModule,
    CommonComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
