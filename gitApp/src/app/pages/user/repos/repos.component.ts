import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GithubApi } from 'src/app/core/api/github/github.api';
import { Observable } from 'rxjs';
import { IColumn } from 'src/app/types/table/IColumn';

@Component({
  selector: 'app-user-repos',
  templateUrl: './repos.component.pug',
  styleUrls: ['./repos.component.styl']
})
export class ReposComponent implements OnInit {
  tableHead = [
    'Id',
    'Name',
    'URL',
    'Created at'
  ];

  tableBody: IColumn[][];

  @Input() username: string;
  @Output() repoClicked: EventEmitter<string> = new EventEmitter<string>();

  repos: any[];

  private _repos$: Observable<any[]>;
  private _currentPage = 0;

  constructor(
    private _githubApi: GithubApi
  ) { }

  async ngOnInit() {
    if (this.username) {
      this._repos$ = this._githubApi.getUserRepos(this.username);
      await this._setBatch();
    }
  }

  getPreviousPage() {
    this._currentPage--;
    if (this._currentPage < 0) {
      this._currentPage = 0;
      return;
    }
    this._setBatch();
  }

  async getNextPage() {
    const repos = await this._repos$.toPromise();
    this._currentPage++;
    if (this._currentPage * 10 >= repos.length) {
      this._currentPage--;
      return;
    }
    this._setBatch();
  }

  private async _setBatch() {
    const repos = await this._repos$.toPromise();

    this.tableBody = repos.slice(
      this._currentPage * 10,
      (this._currentPage + 1) * 10,
    ).map(repo => {
      return [
        {
          value: repo.id
        },
        {
          value: repo.name
        },
        {
          value: repo.html_url,
          link: repo.html_url
        },
        {
          value: repo.created_at
        }
      ];
    });
  }

  clickRepo(url: string): void {
    this.repoClicked.emit(url);
  }
}
