import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { GithubApi } from 'src/app/core/api/github/github.api';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/types/users/IUser';

@Component({
  templateUrl: './user.component.pug',
  styleUrls: ['./user.component.styl']
})
export class UserComponent implements OnInit {

  user$: Observable<IUser>;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _githubApi: GithubApi
  ) { }

  ngOnInit(): void {
    const username = this._activatedRoute.snapshot.params.username;
    if (username) {
      this.user$ = this._githubApi.getUser(username);
    }
  }

  repoClicked(url: string) {
    alert(url);
  }
}
