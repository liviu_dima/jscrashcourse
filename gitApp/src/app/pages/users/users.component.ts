import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

// Api
import { GithubApi } from 'src/app/core/api/github/github.api';
import { IUser } from 'src/app/types/users/IUser';

@Component({
  templateUrl: './users.component.pug',
  styleUrls: ['./users.component.styl']
})
export class UsersComponent implements OnInit {

  tableHead = ['Id', 'Username'];
  tableBody: any[];

  private _page = 0;

  private _users$: Observable<IUser[]> = this._githubApi.getUsers(`${this._page * 30}`);

  constructor(
    private _githubApi: GithubApi
  ) { }

  async ngOnInit() {
    await this._setData();
  }

  /**
   * Fetch previous page users
   */
  getPreviousPage() {
    const newPage = (this._page - 1 < 0) ? 0 : this._page - 1;

    // If it was already on the first page
    // stop execution
    if (newPage === this._page) {
      return;
    }

    this._page = newPage;
    this._updateUsers();
  }

  /**
   * Fetch next page users
   */
  getNextPage() {
    this._page++;
    this._updateUsers();
  }

  /**
   * Update users entries for current page
   */
  private _updateUsers() {
    this._users$ = this._githubApi.getUsers(`${this._page * 30}`);
    this._setData();
  }

  private async _setData() {
    const users = await this._users$.toPromise();
    this.tableBody = users.map(user => {
      return [
        {
          value: user.id
        },
        {
          value: user.login,
          link: `/users/${user.login}`
        }
      ];
    });
  }
}
