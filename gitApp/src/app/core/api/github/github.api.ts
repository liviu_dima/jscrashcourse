import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

// environment
import { environment } from 'src/environments/environment';

// Types
import { IUser } from 'src/app/types/users/IUser';

@Injectable()
export class GithubApi {

  constructor(
    private _http: HttpClient
  ) { }

  private _routes = {
    getUsers: `${environment.baseUrl}/users`
  };

  /**
   * Get users from GitHub
   * Pagination is powered exclusively by the since parameter.
   * @param since The integer ID of the last User that you've seen.
   */
  getUsers(since: string): Observable<IUser[]> {
    const params = new HttpParams()
      .set('since', since);

    try {

      return this._http.get(
        this._routes.getUsers,
        { params: params }
      ) as Observable<IUser[]>;

    } catch (err) {
      console.error(err);
    }
  }

  getUser(username: string): Observable<IUser> {
    try {

      return this._http.get(
        `${this._routes.getUsers}/${username}`
      ) as Observable<IUser>;

    } catch (err) {
      console.error(err);
    }
  }

  getUserRepos(username: string): Observable<any[]> {
    try {

      return this._http.get(
        `${this._routes.getUsers}/${username}/repos`
      ) as Observable<any[]>;

    } catch (err) {
      console.error(err);
    }

  }

}
