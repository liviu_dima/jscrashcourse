export interface IColumn {
  value: string;
  link?: string;
}
