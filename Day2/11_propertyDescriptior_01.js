// "use strict";

var myObject = {};

Object.defineProperty(myObject, "a", {
  value: 4,
  writable: true,
  configurable: true,
  enumerable: true
});

console.log(myObject)

myObject.a = 2;

console.log(myObject)
