function NothingSpecial() {
  console.log("Don't mind me!");
}

const a = new NothingSpecial();
// "Don't mind me!"

a; // {}
