const myObject = {
  foo: function () { // anonymous function that is referenced through property name `foo`
    console.log("foo");
  }
};

const someFoo = myObject.foo;

someFoo();		// function foo(){..}

myObject.foo();	// function foo(){..}
