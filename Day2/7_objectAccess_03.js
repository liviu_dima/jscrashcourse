// As of ES6
const prefix = "foo";

const myObject = {
  [prefix + "bar"]: "hello",
  [prefix + "baz"]: "world"
};

console.log(myObject["foobar"]); // hello
console.log(myObject["foobaz"]); // world
