function foo() {
  console.log(this.a);
}

const obj = {
  a: 2,
  foo: foo
};

// equivalent to 'var a = "oops, global";' in a browser
var a = "oops, global"; // `a` also property on global object

setTimeout(obj.foo, 100); // "oops, global"
