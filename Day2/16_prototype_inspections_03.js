function Foo() {
  // ...
}

Foo.prototype.blah = 'test';

var a = new Foo();

console.log(Object.getPrototypeOf(a) === Foo.prototype);
