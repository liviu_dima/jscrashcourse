function Foo() {
  // ...
}

Foo.prototype.blah = 'test';

var a = new Foo();

console.log(Foo.prototype.isPrototypeOf(a));
