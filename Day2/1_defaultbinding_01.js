function foo() {
  console.log(this.a);
}

// *global* (global.a) context
var a = 2;

foo(); // 2
