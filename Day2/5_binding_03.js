function foo(something) {
  this.a = something;
}

const obj1 = {};

const bar = foo.bind(obj1);
bar(2);
console.log(obj1.a); // 2

const baz = new bar(3);
console.log(obj1.a); // 2
console.log(baz.a); // 3
