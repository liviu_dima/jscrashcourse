const obj1 = {
  a: 2,
  b: 3
};

const obj2 = {
  obj: Object.assign({}, obj1) // ES6
};

obj2.obj.a = 1;

console.log(obj1);
console.log(obj2);
