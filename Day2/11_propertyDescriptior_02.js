// "use strict";

var myObject = {};

Object.defineProperty(myObject, "a", {
  value: 4,
  writable: true,
  configurable: false,
  enumerable: true
});

console.log(myObject)

Object.defineProperty(myObject, "a", {
  value: 3,
  writable: true,
  configurable: true,
  enumerable: true
});

console.log(myObject)
