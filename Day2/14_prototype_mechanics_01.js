function Foo(name) {
  this.name = name;
}

Foo.prototype.myName = function () {
  return this.name;
};

const a = new Foo("a");
const b = new Foo("b");

console.log(a.myName()); // "a"
console.log(b.myName()); // "b"

console.log(Object.getPrototypeOf(a));
