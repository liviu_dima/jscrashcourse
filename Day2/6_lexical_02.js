function foo() {
  const self = this; // lexical capture of `this`
  setTimeout(function () {
    console.log(self.a);
  }, 100);
}

const obj = {
  a: 2
};

foo.call(obj); // 2
