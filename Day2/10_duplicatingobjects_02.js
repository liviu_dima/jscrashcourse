const obj1 = {
  a: 2,
  b: 3
};

const obj2 = {
  obj: obj1
};

obj2.obj.a = 1;

console.log(obj1);
console.log(obj2);
