function foo(something) {
  console.log(this.a, something);
  return this.a + something;
}

const obj = {
  a: 2
};

const bar = foo.bind(obj);

const b = bar(3); // 2 3
console.log(b); // 5
