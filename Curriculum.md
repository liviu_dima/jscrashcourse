# Curriculum

## Day 1

1. Statements
2. Expressions
3. Executing a program
4. Code Comments
5. Blocks
6. Variables
  * Declarations
  * Naming
  * Global variables
7. Data structures and types
  * Data types
  * Objects
    * Arrays
    * Functions
  * Built-In Type Methods
  * Data conversions (coercion)
    * Boolean coercion
      * Falsy Values
      * Falsy Objects
    * The Curious Case of the `~`
    * Explicitly: Parsing Numeric Strings
    * Equality
8. Functions
  * Defining functions
    * Function expression
    * IIFE
    * Arrow function expression
    * Functions Arguments
    * Rest parameters
9. Scope & Clojures
  * Scope
    * Function vs. Block Scope
      * Scope From Functions
    * Blocks As Scopes
  * Closures
10. Hoisting
  * Functions First

References:
- https://github.com/getify/You-Dont-Know-JS/blob/master/up%20%26%20going
- https://github.com/getify/You-Dont-Know-JS/tree/master/scope%20&%20closures
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_Types
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/function
- https://developer.mozilla.org/en-US/docs/Glossary/IIFE
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures

## Day 2

1. This
2. Objects
3. Prototypes
4. Classes

References:
- https://github.com/getify/You-Dont-Know-JS/blob/master/this%20%26%20object%20prototypes
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/prototype

## Day 3

1. Event loop
2. Concurrency
3. Callbacks
4. Promises
5. Async - await

References:
- https://github.com/getify/You-Dont-Know-JS/tree/master/async%20&%20performance
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises

## Day 4

1. Intro to TypeScript
2. Intro to Angular & Angular-CLI (Building a small App)

## Day 5

1. Intro to RXJS (Working with observables)
2. Building a more complex app with Angular and RXJS

References:
- https://stackify.com/typescript-vs-javascript-migrate/
- https://angular.io/guide/architecture
- https://angular.io/guide/reactive-forms
- https://angular.io/guide/observables
