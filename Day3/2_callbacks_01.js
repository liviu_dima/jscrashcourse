const fs = require('fs');

const rootDir = '/Users/lsd/Projects/JsCrashCourse/Day3/callbackFiles/';


// make folders named 1, 2 and so on to 5
// create that specific number of files in each of the folders
// each with the name of 1, 2 and so on...
// each file should contain the name of its own file
// e.g. for file 1, the content should be 1
// for file 2, 2. and so on...
// also, if the file exists, double its content and then add the current file name
function createDirs() {
  for (let i = 1; i <= 5; i++) {
    const dirName = `${rootDir}${i}`;
    // check if directory exists
    fs.stat(dirName, function (err, stats) {
      // if directory does not exists it will return code 'ENOENT'
      if (err && err.code === 'ENOENT') {
        // create directory and files if dir does not exists
        fs.mkdir(dirName, function (err) {
          if (err) throw err;

          createFiles(dirName, i);
        });
      } else { // "else" -> if directory exists create files
        createFiles(dirName, i);
      }
    });
  }
}

function createFiles(dirName, currentDir) {
  for (let j = 1; j <= currentDir; j++) {
    const fileName = `${dirName}/${j}.txt`;

    fs.access(fileName, fs.constants.F_OK, (err) => {

      // file does not exists
      if (err) {
        const data = new Uint8Array(Buffer.from(`${j}`));

        fs.writeFile(fileName, data, (err) => {
          if (err) throw err;

          console.log(`The file ${j}.txt has been saved in directory ${dirName}!`);
        });
        // file exists
      } else {
        fs.readFile(fileName, (err, data) => {
          if (err) throw err;

          // new data concatenates previous content with current content
          const newData = new Uint8Array(Buffer.from(`${data}${j}`));

          fs.writeFile(fileName, newData, (err) => {
            if (err) throw err;

            console.log(`The file ${j}.txt has been saved in directory ${dirName}!`);
          });
        });
      }
    });
  }
}

createDirs();
