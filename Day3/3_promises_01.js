const fs = require('fs');
const fsPromises = fs.promises;

const rootDir = '/Users/lsd/Projects/JsCrashCourse/Day3/promisesFiles/';


// make folders named 1, 2 and so on to 5
// create that specific number of files in each of the folders
// each with the name of 1, 2 and so on...
// each file should contain the name of its own file
// e.g. for file 1, the content should be 1
// for file 2, 2. and so on...
// also, if the file exists, double its content and then add the current file name

function createDirs() {
  for (let i = 1; i <= 5; i++) {
    const dirName = `${rootDir}${i}`;

    fsPromises.stat(dirName)
      .then(() => createFiles(dirName, i))
      .catch((err) => {
        fsPromises.mkdir(dirName)
          .then(() => createFiles(dirName, i))
          .catch(dirErr => console.error(dirErr));
      });
  }
}

function createFiles(dirName, currentDir) {
  for (let j = 1; j <= currentDir; j++) {
    const fileName = `${dirName}/${j}.txt`;

    fsPromises.access(fileName, fs.constants.F_OK)
      .then(() => {
        fsPromises.readFile(fileName)
          .then(data => {
            // new data concatenates previous content with current content
            const newData = new Uint8Array(Buffer.from(`${data}${j}`));

            fsPromises.writeFile(fileName, newData)
              .then(() => console.log(`The file ${j}.txt has been saved in directory ${dirName}!`))
              .catch(err => console.error(err))
          })
          .catch((err) => console.error(err));;
      })
      .catch(() => {
        const data = new Uint8Array(Buffer.from(`${j}`));

        fsPromises.writeFile(fileName, data)
          .then(() => console.log(`The file ${j}.txt has been saved in directory ${dirName}!`))
          .catch(err => console.error(err))
      });
  }
}

createDirs();
