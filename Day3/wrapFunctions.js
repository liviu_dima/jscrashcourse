const fs = require('fs');

const rootDir = '/Users/lsd/Projects/JsCrashCourse/Day3/asyncFiles/';

async function createDirs() {
  for (let i = 1; i <= 5; i++) {
    const dirName = `${rootDir}${i}`;

    try {
      await promiseStat(dirName);
    } catch (err) {
      await promiseMkdir(dirName);
    }

    await createFiles(dirName, i);
  }
}


async function createFiles(dirName, currentDir) {
  for (let j = 1; j <= currentDir; j++) {
    const fileName = `${dirName}/${j}.txt`;

    let data;

    try {
      await promiseAccess(fileName, fs.constants.F_OK);
      data = await promiseRead(fileName);
    } catch {
      data = '';
    }

    const newData = new Uint8Array(Buffer.from(`${data}${j}`));

    promiseWrite(fileName, newData)
      .then(() => console.log(`The file ${j}.txt has been saved in directory ${dirName}!`))
      .catch((err) => console.error(err));
  }
}

const promiseStat = (dirName) => {
  return new Promise((resolve, reject) => {
    fs.stat(dirName, function (err, stats) {
      // if directory does not exists it will return code 'ENOENT'
      if (err) {
        return reject(err);
      }

      resolve(stats);
    });
  });
}

const promiseMkdir = (dirName) => {
  return new Promise((resolve, reject) => {
    fs.mkdir(dirName, function (err) {
      if (err) {
        return reject(err);
      };

      resolve();
    });
  });
}

const promiseAccess = (fileName, status) => {
  return new Promise((resolve, reject) => {
    fs.access(fileName, fs.constants.F_OK, (err) => {

      // file does not exists
      if (err) {
        return reject(err);
      }

      resolve();
    });
  });
}

const promiseRead = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, (err, data) => {
      if (err) {
        return reject(err);
      };

      resolve(data);
    });
  });
}

const promiseWrite = (fileName, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, data, (err) => {
      if (err) {
        return reject(err);
      };

      resolve();
    });
  });
}

createDirs();
