const fs = require('fs');
const fsPromises = fs.promises;

const rootDir = '/Users/lsd/Projects/JsCrashCourse/Day3/asyncFiles/';


// make folders named 1, 2 and so on to 5
// create that specific number of files in each of the folders
// each with the name of 1, 2 and so on...
// each file should contain the name of its own file
// e.g. for file 1, the content should be 1
// for file 2, 2. and so on...
// also, if the file exists, double its content and then add the current file name

async function createDirs() {
  for (let i = 1; i <= 5; i++) {
    const dirName = `${rootDir}${i}`;

    try {
      await fsPromises.stat(dirName);
    } catch (err) {
      await fsPromises.mkdir(dirName);
    }

    await createFiles(dirName, i);
  }
}

async function createFiles(dirName, currentDir) {
  for (let j = 1; j <= currentDir; j++) {
    const fileName = `${dirName}/${j}.txt`;

    let data;

    try {
      await fsPromises.access(fileName, fs.constants.F_OK);
      data = await fsPromises.readFile(fileName);
    } catch {
      data = '';
    }

    const newData = new Uint8Array(Buffer.from(`${data}${j}`));

    try {
      await fsPromises.writeFile(fileName, newData);
      console.log(`The file ${j}.txt has been saved in directory ${dirName}!`);
    } catch (err) {
      console.error(err)
    }
  }
}

createDirs();
