const a = 42;
const b = "42";

console.log(a === b);	// false
// equivalent to a == Number(b)
console.log(a == b);		// true
