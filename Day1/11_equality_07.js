const a = null;
const b = Object(a);
console.log(a == b);

const c = undefined;
const d = Object(c);
console.log(c == d);

const e = NaN;
const f = Object(e);
console.log(e == f);
