const a = "test";

console.log(typeof a);
console.log(a.toUpperCase());
console.log(a);

const b = new String("test");

console.log(typeof b);
console.log(b.toUpperCase());
console.log(b);
