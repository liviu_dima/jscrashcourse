function getDiscountsValue(totalValue, ...discountsPercent) {

  return discountsPercent.map(discount => totalValue * discount / 100)

}

const orderDiscount = 10;
const promotionalDiscount = 25;

console.log(getDiscountsValue(350, orderDiscount, promotionalDiscount));
