const a = new Boolean(false);
const b = new Number(0);
const c = new String("");

const d = Boolean(a && b && c);

console.log(d); // true
