var add5 = function (x) { return x + 5 };
var add10 = function (x) { return x + 10 };

console.log(add5(2));  // 7
console.log(add10(2)); // 12
