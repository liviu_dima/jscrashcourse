getDiscountValue = (discountPercent, totalValue) => {
  return discountPercent / 100 * totalValue
};

console.log(getDiscountValue(5, 350));
