const a = [0, 1, 2, 3, 4, 5, 6];

if (~a.indexOf(4)) {
  console.log('Array "a" contains element "4". ');
} else {
  console.log('Array "a" does not contains element "4". ');
}

if (~a.indexOf(10)) {
  console.log('Array "a" contains element "10". ');
} else {
  console.log('Array "a" does not contains element "10". ');
}

