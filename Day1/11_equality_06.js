const a = "abc";
const b = Object(a);	// same as `new String( a )`

console.log(a === b);
console.log(a == b);
