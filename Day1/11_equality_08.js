console.log("0" == null);			   // false
console.log("0" == undefined);   // false
console.log("0" == false);		   // true -- UH OH!
console.log("0" == NaN);			   // false
console.log("0" == 0);				   // true
console.log("0" == "");				   // false

console.log(false == null);			 // false
console.log(false == undefined); // false
console.log(false == NaN);			 // false
console.log(false == 0);				 // true -- UH OH!
console.log(false == "");			   // true -- UH OH!
console.log(false == []);			   // true -- UH OH!
console.log(false == {});			   // false

console.log("" == null);			   // false
console.log("" == undefined);	   // false
console.log("" == NaN);				   // false
console.log("" == 0);				     // true -- UH OH!
console.log("" == []);				   // true -- UH OH!
console.log("" == {});				   // false

console.log(0 == null);				   // false
console.log(0 == undefined);	   // false
console.log(0 == NaN);				   // false
console.log(0 == []);				     // true -- UH OH!
console.log(0 == {});				     // false
