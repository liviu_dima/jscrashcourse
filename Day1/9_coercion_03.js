const a = 42;

console.log(a);
console.log(typeof a);

const b = a.toString();

console.log(b);
console.log(typeof b);

const c = '43';

console.log(c);
console.log(typeof c);

const d = c.toNumber();

console.log(d);
console.log(typeof d);
