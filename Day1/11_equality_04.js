const a = null;
let b;

// in case of coercion null and undefined are only equal to each other
console.log(a == b);
console.log(a == null);
console.log(b == null);

console.log(a == false);
console.log(b == false);
console.log(a == "");
console.log(b == "");
console.log(a == 0);
console.log(b == 0);
