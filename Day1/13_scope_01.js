function foo(a) {
  const b = a;
  return a + b;
}

const c = foo(2);

console.log(c);
